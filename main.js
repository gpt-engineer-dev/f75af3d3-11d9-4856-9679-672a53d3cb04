// A plausible function to fetch yield curve data from an API
async function fetchYieldCurveData() {
  // Placeholder for API call
  // Replace with actual API endpoint and logic to fetch data
  // Assuming the correct API endpoint is "/v1/accounting/od/debt_to_the_penny" based on the documentation
  // Placeholder for API call
  // Replace with actual API endpoint and logic to fetch data
  return {}; // Return an empty object for now
}

// Function to render the yield curve chart
function renderYieldCurveChart(data) {
  const ctx = document.getElementById("yield-curve-chart").getContext("2d");
  // Extract maturities and yields from the data object
  const maturities = Object.keys(data)
    .filter((key) => key.startsWith("yield_"))
    .map((key) => key.replace("yield_", "").toUpperCase());
  const yields = maturities.map((maturity) => data[`yield_${maturity.toLowerCase()}`]);

  // Create the chart with the extracted data
  const chart = new Chart(ctx, {
    type: "line",
    data: {
      labels: maturities, // Maturities extracted from the data object
      datasets: [
        {
          label: "Yield",
          data: yields, // Yields extracted from the data object
          backgroundColor: "rgba(59, 130, 246, 0.5)",
          borderColor: "rgba(37, 99, 235, 1)",
          borderWidth: 2,
          fill: false,
        },
      ],
    },
    options: {
      scales: {
        y: {
          beginAtZero: false,
        },
      },
    },
  });
}

// Main function to setup the chart
async function setupYieldCurveChart() {
  const yieldCurveData = await fetchYieldCurveData();
  renderYieldCurveChart(yieldCurveData);
}

// Initialize the chart
setupYieldCurveChart();
